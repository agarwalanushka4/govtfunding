/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { MoneyContract } = require('..');
const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logger = {
            getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }

}

describe('MoneyContract', () => {

    let contract;
    let ctx;

    beforeEach(() => {
        contract = new MoneyContract();
        ctx = new TestContext();
        ctx.stub.getState.withArgs('txn-1.1').resolves(Buffer.from('{"value":"money txn-1.1 value"}'));
        ctx.stub.getState.withArgs('txn-1.2').resolves(Buffer.from('{"value":"money txn-1.2 value"}'));
    });

    describe('#moneyExists', () => {

        it('should return true for a money', async () => {
            await contract.moneyExists(ctx, 'txn-1.1').should.eventually.be.true;
        });

        it('should return false for a money that does not exist', async () => {
            await contract.moneyExists(ctx, 'txn-2').should.eventually.be.false;
        });

    });

    describe('#createMoney', () => {

        it('should create a money', async () => {
            await contract.createMoney(ctx, 'txn-1.2', 'money txn-1.2 value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"money txn-1.1 value"}'));
        });

        it('should throw an error for a money that already exists', async () => {
            await contract.createMoney(ctx, 'txn-1.1', 'myvalue').should.be.rejectedWith(/The money txn-1.1 already exists/);
        });

    });

    describe('#readMoney', () => {

        it('should return a money', async () => {
            await contract.readMoney(ctx, 'txn-1.1').should.eventually.deep.equal({ value: 'money txn-1.1 value' });
        });

        it('should throw an error for a money that does not exist', async () => {
            await contract.readMoney(ctx, 'txn-1.5').should.be.rejectedWith(/The money txn-1.5 does not exist/);
        });

    });

    /*describe('#updateMoney', () => {

        it('should update a money', async () => {
            await contract.updateMoney(ctx, '1001', 'money 1001 new value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"money 1001 new value"}'));
        });

        it('should throw an error for a money that does not exist', async () => {
            await contract.updateMoney(ctx, '1003', 'money 1003 new value').should.be.rejectedWith(/The money 1003 does not exist/);
        });

    });*/

    /*describe('#deleteMoney', () => {

        it('should delete a money', async () => {
            await contract.deleteMoney(ctx, '1001');
            ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
        });

        it('should throw an error for a money that does not exist', async () => {
            await contract.deleteMoney(ctx, '1003').should.be.rejectedWith(/The money 1003 does not exist/);
        });

    });*/

});
