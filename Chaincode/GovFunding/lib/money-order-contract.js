/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
//const crypto = require('crypto');

async function getCollectionName(ctx) {
    const collectionName = 'CollectionMoneyOrder';
    return collectionName;
}

class MoneyOrderContract extends Contract {

    async moneyOrderExists(ctx, moneyOrderId) {
        const collectionName = await getCollectionName(ctx);
        const data = await ctx.stub.getPrivateDataHash(collectionName, moneyOrderId);
        return (!!data && data.length > 0);
    }

    async createMoneyOrder(ctx, moneyOrderId) {
        const mspID = ctx.clientIdentity.getMSPID();
        if(mspID === 'GovernmentMSP'){

        const exists = await this.moneyOrderExists(ctx, moneyOrderId);
        if (exists) {
            throw new Error(`The asset money order ${moneyOrderId} already exists`);
        }

        const moneyorderAsset = {};

        const transientData = ctx.stub.getTransient();
        if (transientData.size === 0 || !transientData.has('value')) {
            throw new Error('The privateValue key was not specified in transient data. Please try again.');
        }
        moneyorderAsset.value = transientData.get('value').toString();
        moneyorderAsset.assetType = 'moneyorder';

        const collectionName = await getCollectionName(ctx);
        await ctx.stub.putPrivateData(collectionName, moneyOrderId, Buffer.from(JSON.stringify(moneyorderAsset)));
        }
        else{
            return ('Under following MSP: ${mspID} this action cannot be performed.');
        }
    }

    async readMoneyOrder(ctx, moneyOrderId) {
        const exists = await this.moneyOrderExists(ctx, moneyOrderId);
        if (!exists) {
            throw new Error(`The asset money order ${moneyOrderId} does not exist`);
        }
        let privateDataString;
        const collectionName = await getCollectionName(ctx);
        const privateData = await ctx.stub.getPrivateData(collectionName, moneyOrderId);
        privateDataString = JSON.parse(privateData.toString());
        return privateDataString;
    }

   /* async updateMoneyOrder(ctx, moneyOrderId) {
        const exists = await this.moneyOrderExists(ctx, moneyOrderId);
        if (!exists) {
            throw new Error(`The asset money order ${moneyOrderId} does not exist`);
        }
        const privateAsset = {};

        const transientData = ctx.stub.getTransient();
        if (transientData.size === 0 || !transientData.has('privateValue')) {
            throw new Error('The privateValue key was not specified in transient data. Please try again.');
        }
        privateAsset.privateValue = transientData.get('privateValue').toString();

        const collectionName = await getCollectionName(ctx);
        await ctx.stub.putPrivateData(collectionName, moneyOrderId, Buffer.from(JSON.stringify(privateAsset)));
    }

    async deleteMoneyOrder(ctx, moneyOrderId) {
        const exists = await this.moneyOrderExists(ctx, moneyOrderId);
        if (!exists) {
            throw new Error(`The asset money order ${moneyOrderId} does not exist`);
        }
        const collectionName = await getCollectionName(ctx);
        await ctx.stub.deletePrivateData(collectionName, moneyOrderId);
    }

    async verifyMoneyOrder(ctx, mspid, moneyOrderId, objectToVerify) {

        // Convert provided object into a hash
        const hashToVerify = crypto.createHash('sha256').update(objectToVerify).digest('hex');
        const pdHashBytes = await ctx.stub.getPrivateDataHash(`_implicit_org_${mspid}`, moneyOrderId);
        if (pdHashBytes.length === 0) {
            throw new Error('No private data hash with the key: ' + moneyOrderId);
        }

        const actualHash = Buffer.from(pdHashBytes).toString('hex');

        // Compare the hash calculated (from object provided) and the hash stored on public ledger
        if (hashToVerify === actualHash) {
            return true;
        } else {
            return false;
        }
    }*/


}

module.exports = MoneyOrderContract;
