/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class MoneyContract extends Contract {

    async moneyExists(ctx, moneyId) {
        const buffer = await ctx.stub.getState(moneyId);
        return (!!buffer && buffer.length > 0);
    }

    async createmoney(ctx, moneyId, value) {
        const mspID = ctx.clientIdentity.getMSPID();
        if(mspID === 'GovernmentMSP'){
        const exists = await this.moneyExists(ctx, moneyId);
        if (exists) {
            throw new Error(`The money ${moneyId} already exists`);
        }
        const asset = { value };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(moneyId, buffer);
        }
        else{
            return ('Under following MSP: ${mspID} this action cannot be performed.');
        }
    }

    async readmoney(ctx, moneyId) {
        const exists = await this.moneyExists(ctx, moneyId);
        if (!exists) {
            throw new Error(`The money ${moneyId} does not exist`);
        }
        const buffer = await ctx.stub.getState(moneyId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async queryAllmoney(ctx) {
        const queryString = {
            selector: {
                assetType: 'moneyId',
            },
            sort: [{ dateOfIssue: 'asc' }],
        };
        let resultIterator = await ctx.stub.getQueryResult(
            JSON.stringify(queryString)
        );
        let result = await this.getAllResults(resultIterator, false);
        return JSON.stringify(result);
    }

    async getmoneyByRange(ctx, startKey, endKey) {
        let resultIterator = await ctx.stub.getStateByRange(startKey, endKey);
        let result = await this.getAllResults(resultIterator, false);
        return JSON.stringify(result);
    }

    async getmoneyWithPagination(ctx, _pageSize, _bookmark) {
        const queryString = {
            selector: {
                assetType: 'car',
            },
        };

        const pageSize = parseInt(_pageSize, 10);
        const bookmark = _bookmark;

        const { iterator, metadata } = await ctx.stub.getQueryResultWithPagination(
            JSON.stringify(queryString),
            pageSize,
            bookmark
        );

        const result = await this.getAllResults(iterator, false);

        const results = {};
        results.Result = result;
        results.ResponseMetaData = {
            RecordCount: metadata.fetched_records_count,
            Bookmark: metadata.bookmark,
        };
        return JSON.stringify(results);
    }

    async getmoneyHistory(ctx, moneyId) {
        let resultsIterator = await ctx.stub.getHistoryForKey(moneyId);
        let results = await this.getAllResults(resultsIterator, true);
        return JSON.stringify(results);
    }

    async getAllResults(iterator, isHistory) {
        let allResult = [];

        for (
            let res = await iterator.next();
            !res.done;
            res = await iterator.next()
        ) {
            if (res.value && res.value.value.toString()) {
                let jsonRes = {};

                if (isHistory && isHistory === true) {
                    jsonRes.TxId = res.value.tx_id;
                    jsonRes.timestamp = res.value.timestamp;
                    jsonRes.Value = JSON.parse(res.value.value.toString());
                } else {
                    jsonRes.Key = res.value.key;
                    jsonRes.Record = JSON.parse(res.value.value.toString());
                }
                allResult.push(jsonRes);
            }
        }
        await iterator.close();
        return allResult;
    }

}

module.exports = MoneyContract;
