/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const MoneyContract = require('./lib/money-contract');
const MoneyOrderContract = require('./lib/money-order-contract');

module.exports.MoneyContract = MoneyContract;
module.exports.MoneyOrderContract = MoneyOrderContract;
module.exports.contracts = [ MoneyContract,MoneyOrderContract ];
