#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
version: '2'

networks:
  funding:

services:
  couchdbGovernmentPeer0:
    container_name: couchdbGovernmentPeer0
    image: hyperledger/fabric-couchdb
    environment:
      - COUCHDB_USER=peer0.Government
      - COUCHDB_PASSWORD=password
    ports:
      - 5984:5984
    networks:
      - funding

  couchdbBankPeer0:
    container_name: couchdbBankPeer0
    image: hyperledger/fabric-couchdb
    environment:
      - COUCHDB_USER=peer0.Bank
      - COUCHDB_PASSWORD=password
    ports:
      - 7984:5984
    networks:
      - funding

  couchdbTargetPeer0:
    container_name: couchdbTargetPeer0
    image: hyperledger/fabric-couchdb
    environment:
      - COUCHDB_USER=peer0.Target
      - COUCHDB_PASSWORD=password
    ports:
      - 9984:5984
    networks:
      - funding

  orderer.funds.com:
    container_name: orderer.funds.com
    image: hyperledger/fabric-orderer:2.2.3
    environment:
      - FABRIC_LOGGING_SPEC=INFO
      - ORDERER_GENERAL_LISTENADDRESS=0.0.0.0
      - ORDERER_GENERAL_LISTENPORT=7050
      - ORDERER_GENERAL_GENESISMETHOD=file
      - ORDERER_GENERAL_GENESISFILE=/var/hyperledger/orderer/orderer.genesis.block
      - ORDERER_GENERAL_LOCALMSPID=OrdererMSP
      - ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/orderer/msp
      - ORDERER_GENERAL_TLS_ENABLED=true
      - ORDERER_GENERAL_TLS_PRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_TLS_CERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_TLS_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
      - ORDERER_GENERAL_CLUSTER_CLIENTCERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_CLUSTER_CLIENTPRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_CLUSTER_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/orderer
    command: orderer
    ports:
      - 7050:7050
    volumes:
      - ../channel-artifacts/genesis.block:/var/hyperledger/orderer/orderer.genesis.block
      - ../organizations/ordererOrganizations/funds.com/orderer.funds.com/orderers/orderer.funds.com/msp:/var/hyperledger/orderer/msp
      - ../organizations/ordererOrganizations/funds.com/orderer.funds.com/orderers/orderer.funds.com/tls:/var/hyperledger/orderer/tls
    networks:
      - funding

  peer0.government.funds.com:
    container_name: peer0.government.funds.com
    image: hyperledger/fabric-peer:2.2.3
    environment:
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_PEER_ID=peer0.government.funds.com
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_LISTENADDRESS=0.0.0.0:7051
      - CORE_PEER_CHAINCODEADDRESS=peer0.government.funds.com:7052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7052
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer0.government.funds.com:7051
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.government.funds.com:7051
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      - CORE_CHAINCODE_LOGGING_LEVEL=INFO
      - CORE_PEER_LOCALMSPID=GovernmentMSP
      - CORE_PEER_ADDRESS=peer0.government.funds.com:7051
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=${COMPOSE_PROJECT_NAME}_funding
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdbGovernmentPeer0:5984
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=peer0.Government
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=password
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: peer node start
    ports:
      - 7051:7051
      - 7053:7053
    volumes:
      - /var/run/:/host/var/run/
      - ../organizations/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/msp:/etc/hyperledger/fabric/msp
      - ../organizations/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls:/etc/hyperledger/fabric/tls
      
    depends_on:
      - orderer.funds.com
      - couchdbGovernmentPeer0  
    networks:
      - funding

  peer0.bank.funds.com:
    container_name: peer0.bank.funds.com
    image: hyperledger/fabric-peer:2.2.3
    environment:
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_PEER_ID=peer0.bank.funds.com
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_LISTENADDRESS=0.0.0.0:9051
      - CORE_PEER_CHAINCODEADDRESS=peer0.bank.funds.com:9052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:9052
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer0.bank.funds.com:9051
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.bank.funds.com:9051
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      - CORE_CHAINCODE_LOGGING_LEVEL=INFO
      - CORE_PEER_LOCALMSPID=BankMSP
      - CORE_PEER_ADDRESS=peer0.bank.funds.com:9051
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=${COMPOSE_PROJECT_NAME}_funding
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdbBankPeer0:5984
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=peer0.Bank
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=password
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: peer node start
    ports:
      - 9051:9051
      - 9053:9053
    volumes:
      - /var/run/:/host/var/run/
      - ../organizations/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/msp:/etc/hyperledger/fabric/msp
      - ../organizations/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls:/etc/hyperledger/fabric/tls
      
    depends_on:
      - orderer.funds.com
      - couchdbBankPeer0  
    networks:
      - funding 

  peer0.target.funds.com:
    container_name: peer0.target.funds.com
    image: hyperledger/fabric-peer:2.2.3
    environment:
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_PEER_ID=peer0.target.funds.com
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_LISTENADDRESS=0.0.0.0:11051
      - CORE_PEER_CHAINCODEADDRESS=peer0.target.funds.com:11052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:11052
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer0.target.funds.com:11051
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.target.funds.com:11051
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      - CORE_CHAINCODE_LOGGING_LEVEL=INFO
      - CORE_PEER_LOCALMSPID=TargetMSP
      - CORE_PEER_ADDRESS=peer0.target.funds.com:11051
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=${COMPOSE_PROJECT_NAME}_funding
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdbTargetPeer0:5984
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=peer0.Target
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=password
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: peer node start
    ports:
      - 11051:11051
      - 11053:11053
    volumes:
      - /var/run/:/host/var/run/
      - ../organizations/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/msp:/etc/hyperledger/fabric/msp
      - ../organizations/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls:/etc/hyperledger/fabric/tls
      
    depends_on:
      - orderer.funds.com
      - couchdbTargetPeer0  
    networks:
      - funding    

  cli:
    container_name: cli
    image: hyperledger/fabric-tools:2.2.3
    tty: true
    environment:
      - GOPATH=/opt/gopath
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_ID=cli
      - CORE_PEER_ADDRESS=peer0.government.funds.com:7051
      - CORE_PEER_LOCALMSPID=GovernmentMSP
      - CORE_CHAINCODE_KEEPALIVE=10
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/ca.crt
      - CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/users/Admin@government.funds.com/msp
      - ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: /bin/bash
    volumes:
      - /var/run/:/host/var/run/
      - ../../Chaincode/:/opt/gopath/src/github.com/chaincode/
      - ../organizations:/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/
      - ../channel-artifacts:/opt/gopath/src/github.com/hyperledger/fabric/peer/config/
    networks:
        - funding
    depends_on:
      - orderer.funds.com
      - peer0.government.funds.com
      - peer0.bank.funds.com
      - peer0.target.funds.com

