#!/bin/bash


sudo rm -rf \
    ./organizations/fabric-ca/bank/msp \
    ./organizations/fabric-ca/bank/ca-cert.pem \
    ./organizations/fabric-ca/bank/fabric-ca-server.db \
    ./organizations/fabric-ca/bank/IssuerPublicKey \
    ./organizations/fabric-ca/bank/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/bank/tls-cert.pem

sudo rm -rf \
    ./organizations/fabric-ca/government/msp \
    ./organizations/fabric-ca/government/ca-cert.pem \
    ./organizations/fabric-ca/government/fabric-ca-server.db \
    ./organizations/fabric-ca/government/IssuerPublicKey \
    ./organizations/fabric-ca/government/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/government/tls-cert.pem

sudo rm -rf \
    ./organizations/fabric-ca/target/msp \
    ./organizations/fabric-ca/target/ca-cert.pem \
    ./organizations/fabric-ca/target/fabric-ca-server.db \
    ./organizations/fabric-ca/target/IssuerPublicKey \
    ./organizations/fabric-ca/target/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/target/tls-cert.pem

sudo rm -rf \
    ./organizations/fabric-ca/orderer/msp \
    ./organizations/fabric-ca/orderer/ca-cert.pem \
    ./organizations/fabric-ca/orderer/fabric-ca-server.db \
    ./organizations/fabric-ca/orderer/IssuerPublicKey \
    ./organizations/fabric-ca/orderer/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/orderer/tls-cert.pem

sudo rm -rf \
    ./organizations/ordererOrganizations/funds.con/orderer.funds.con/msp \
    ./organizations/ordererOrganizations/funds.con/orderer.funds.con/fabric-ca-client-config.yaml \
    ./organizations/ordererOrganizations/funds.con/orderer.funds.con/orderers \
    ./organizations/ordererOrganizations/funds.con/orderer.funds.con/users
    
sudo rm -rf \
    ./organizations/peerOrganizations/funds.con/government.funds.con/ca \
    ./organizations/peerOrganizations/funds.con/government.funds.con/msp \
    ./organizations/peerOrganizations/funds.con/government.funds.con/peers \
    ./organizations/peerOrganizations/funds.con/government.funds.con/tlsca \
    ./organizations/peerOrganizations/funds.con/government.funds.con/users \
    ./organizations/peerOrganizations/funds.con/government.funds.con/fabric-ca-client-config.yaml

sudo rm -rf \
    ./organizations/peerOrganizations/funds.con/bank.funds.con/ca \
    ./organizations/peerOrganizations/funds.con/bank.funds.con/msp \
    ./organizations/peerOrganizations/funds.con/bank.funds.con/peers \
    ./organizations/peerOrganizations/funds.con/bank.funds.con/tlsca \
    ./organizations/peerOrganizations/funds.con/bank.funds.con/users \
    ./organizations/peerOrganizations/funds.con/bank.funds.con/fabric-ca-client-config.yaml

sudo rm -rf \
    ./organizations/peerOrganizations/funds.con/target.funds.con/ca \
    ./organizations/peerOrganizations/funds.con/target.funds.con/msp \
    ./organizations/peerOrganizations/funds.con/target.funds.con/peers \
    ./organizations/peerOrganizations/funds.con/target.funds.con/tlsca \
    ./organizations/peerOrganizations/funds.con/target.funds.con/users \
    ./organizations/peerOrganizations/funds.con/target.funds.con/fabric-ca-client-config.yaml