sh createCa.sh

sleep 5

echo "########## Setting required env ############"
export CHANNEL_NAME=govtchannel
export IMAGE_TAG=latest
export COMPOSE_PROJECT_NAME=fabricscratch-ca

echo "########## Generate the genesis block ############"
configtxgen -profile OrdererGenesis \
-channelID system-channel -outputBlock \
./channel-artifacts/genesis.block

sleep 2

echo "########## Generate the Channel Transaction ############"
configtxgen -profile GovtChannel \
-outputCreateChannelTx ./channel-artifacts/$CHANNEL_NAME.tx \
-channelID $CHANNEL_NAME

sleep 2

echo "########## Starting the components ############"
docker-compose -f docker/docker-compose-singlepeer.yml up -d

export ORDERER_TLS_CA=`docker exec cli  env | grep ORDERER_TLS_CA | cut -d'=' -f2`

sleep 2

echo "########## Creating the Channel ############"
docker exec cli peer channel create -o orderer.funds.com:7050 \
-c $CHANNEL_NAME -f /opt/gopath/src/github.com/hyperledger/fabric/peer/config/$CHANNEL_NAME.tx \
--tls --cafile $ORDERER_TLS_CA


sleep 2

echo "########## Joining Government Peer to Channel ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=GovernmentMSP \
     -e CHANNEL_NAME=govtchannel \
     -e CORE_PEER_ADDRESS=peer0.government.funds.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/users/Admin@government.funds.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel join -b $CHANNEL_NAME.block

sleep 2

echo "########## Joining Bank Peer to Channel ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=BankMSP \
     -e CHANNEL_NAME=govtchannel \
     -e CORE_PEER_ADDRESS=peer0.bank.funds.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/users/Admin@bank.funds.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel join -b $CHANNEL_NAME.block

sleep 2

echo "########## Joining Target Peer to Channel ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=TargetMSP \
     -e CHANNEL_NAME=govtchannel \
     -e CORE_PEER_ADDRESS=peer0.target.funds.com:11051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/target.funds.com/users/Admin@target.funds.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel join -b $CHANNEL_NAME.block

sleep 2

echo "########## Generating anchor peer tx for government ############"
configtxgen -profile GovtChannel -outputAnchorPeersUpdate ./channel-artifacts/GovernmentMSPanchors.tx -channelID govtchannel -asOrg GovernmentMSP

sleep 2

echo "########## Generating anchor peer tx for bank ############"
configtxgen -profile GovtChannel -outputAnchorPeersUpdate ./channel-artifacts/BankMSPanchors.tx -channelID govtchannel -asOrg BankMSP

sleep 2

echo "########## Generating anchor peer tx for bank ############"
configtxgen -profile GovtChannel -outputAnchorPeersUpdate ./channel-artifacts/TargetMSPanchors.tx -channelID govtchannel -asOrg TargetMSP

sleep 2

echo "########## Anchor Peer Update for Government ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=GovernmentMSP \
     -e CHANNEL_NAME=govtchannel \
     -e CORE_PEER_ADDRESS=peer0.government.funds.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/users/Admin@government.funds.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel update -o orderer.funds.com:7050 -c govtchannel -f ./config/GovernmentMSPanchors.tx --tls --cafile $ORDERER_TLS_CA

sleep 2

echo "##########  Anchor Peer Update for Bank ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=BankMSP \
     -e CHANNEL_NAME=govtchannel \
     -e CORE_PEER_ADDRESS=peer0.bank.funds.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/users/Admin@bank.funds.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel update -o orderer.funds.com:7050 -c govtchannel -f ./config/BankMSPanchors.tx --tls --cafile $ORDERER_TLS_CA

sleep 2

echo "##########  Anchor Peer Update for Target ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=TargetMSP \
     -e CHANNEL_NAME=govtchannel \
     -e CORE_PEER_ADDRESS=peer0.target.funds.com:11051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/target.funds.com/users/Admin@target.funds.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel update -o orderer.funds.com:7050 -c govtchannel -f ./config/TargetMSPanchors.tx --tls --cafile $ORDERER_TLS_CA

sleep 2

echo "##########  Package Chaincode ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=GovernmentMSP \
     -e CHANNEL_NAME=govtchannel \
     -e CORE_PEER_ADDRESS=peer0.government.funds.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/users/Admin@government.funds.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode package govfunds.tar.gz --path /opt/gopath/src/github.com/chaincode/GovFunding/ --lang node --label govfunds_1

sleep 2

echo "##########  Install Chaincode on Government peer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=GovernmentMSP \
     -e CHANNEL_NAME=govtchannel \
     -e CORE_PEER_ADDRESS=peer0.government.funds.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/users/Admin@government.funds.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode install govfunds.tar.gz


sleep 2

echo "##########  Install Chaincode on Bank peer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=BankMSP \
     -e CHANNEL_NAME=govtchannel \
     -e CORE_PEER_ADDRESS=peer0.bank.funds.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/users/Admin@bank.funds.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode install govfunds.tar.gz

sleep 2

echo "##########  Install Chaincode on Target peer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=TargetMSP \
     -e CHANNEL_NAME=govtchannel \
     -e CORE_PEER_ADDRESS=peer0.target.funds.com:11051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/target.funds.com/users/Admin@target.funds.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode install govfunds.tar.gz


echo "##########  Copy the above package ID for next steps, follow the approveCommit.txt ############"
