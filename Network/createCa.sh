echo "##########Strating the docker-compose-ca############"
export COMPOSE_PROJECT_NAME=fabricscratch-ca
docker-compose -f ./docker/docker-compose-ca.yml up -d 

echo "##########Creating the folder structure for CA############"

mkdir -p organizations/peerOrganizations/funds.com/government.funds.com/
mkdir -p organizations/peerOrganizations/funds.com/bank.funds.com/
mkdir -p organizations/peerOrganizations/funds.com/target.funds.com/

echo "##########Setting PATH for Fabric CA client############"

export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/funds.com/government.funds.com/
sleep 2

echo "########## Enroll Government ca admin############"

fabric-ca-client enroll -u https://admin:adminpw@localhost:7054 --caname ca-government --tls.certfiles ${PWD}/organizations/fabric-ca/government/tls-cert.pem

sleep 5

echo "########## Create config.yaml for Government ############"

echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-government.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-government.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-government.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-government.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/funds.com/government.funds.com/msp/config.yaml

echo "########## Register government peer ############"
fabric-ca-client register --caname ca-government --id.name peer0government --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/government/tls-cert.pem
sleep 5

echo "########## Register government user ############"

fabric-ca-client register --caname ca-government --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/government/tls-cert.pem
sleep 5

echo "########## Register government Org admin ############"

fabric-ca-client register --caname ca-government --id.name governmentadmin --id.secret governmentadminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/government/tls-cert.pem
sleep 5

echo "########## Generate Government peer MSP ############"

fabric-ca-client enroll -u https://peer0government:peer0pw@localhost:7054 --caname ca-government -M ${PWD}/organizations/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/msp --csr.hosts peer0.government.funds.com --tls.certfiles ${PWD}/organizations/fabric-ca/government/tls-cert.pem
sleep 5

echo "########## Generate Government Peer tls cert ############"

fabric-ca-client enroll -u https://peer0government:peer0pw@localhost:7054 --caname ca-government -M ${PWD}/organizations/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls --enrollment.profile tls --csr.hosts peer0.government.funds.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/government/tls-cert.pem
sleep 5

echo "########## Organizing the folders ############"
echo "########## Copy the certificate files ############"

cp organizations/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/tlscacerts/* organizations/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/ca.crt
cp organizations/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/signcerts/* organizations/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.crt
cp organizations/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/keystore/* organizations/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.key

mkdir -p ${PWD}/organizations/peerOrganizations/funds.com/government.funds.com/msp/tlscacerts
cp ${PWD}/organizations/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/funds.com/government.funds.com/msp/tlscacerts/ca.crt

mkdir -p ${PWD}/organizations/peerOrganizations/funds.com/government.funds.com/tlsca
cp ${PWD}/organizations/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/funds.com/government.funds.com/tlsca/tls-localhost-7054-ca-government.pem

mkdir -p ${PWD}/organizations/peerOrganizations/funds.com/government.funds.com/ca
cp ${PWD}/organizations/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/funds.com/government.funds.com/ca/localhost-7054-ca-government.pem

cp organizations/peerOrganizations/funds.com/government.funds.com/msp/config.yaml organizations/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/msp/config.yaml

echo "Generate User MSP"
echo "================="

fabric-ca-client enroll -u https://user1:user1pw@localhost:7054 --caname ca-government -M ${PWD}/organizations/peerOrganizations/funds.com/government.funds.com/users/User1@government.funds.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/government/tls-cert.pem

sleep 5

cp organizations/peerOrganizations/funds.com/government.funds.com/msp/config.yaml organizations/peerOrganizations/funds.com/government.funds.com/users/User1@government.funds.com/msp/config.yaml

echo "Generate org Admin MSP"
echo "======================"

fabric-ca-client enroll -u https://governmentadmin:governmentadminpw@localhost:7054 --caname ca-government -M ${PWD}/organizations/peerOrganizations/funds.com/government.funds.com/users/Admin@government.funds.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/government/tls-cert.pem
sleep 5

cp ${PWD}/organizations/peerOrganizations/funds.com/government.funds.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/funds.com/government.funds.com/users/Admin@government.funds.com/msp/config.yaml

echo "============================================================End of Government =================================================================================================="

echo "================================================================== Bank =================================================================================================="


export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/funds.com/bank.funds.com/

echo "enroll Bank ca"
echo "======================"

fabric-ca-client enroll -u https://admin:adminpw@localhost:8054 --caname ca-bank --tls.certfiles ${PWD}/organizations/fabric-ca/bank/tls-cert.pem

sleep 5




echo "Create config.yaml for Bank" 
echo "==================================="

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-bank.pem 
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-bank.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-bank.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-bank.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/funds.com/bank.funds.com/msp/config.yaml

echo "Register bank peer"
echo "=========================="
fabric-ca-client register --caname ca-bank --id.name peer0bank --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/bank/tls-cert.pem
sleep 5

echo "Register bank user"
echo "=========================="
fabric-ca-client register --caname ca-bank --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/bank/tls-cert.pem
sleep 5

echo "Register bank Org admin"
echo "=========================="
fabric-ca-client register --caname ca-bank --id.name bankadmin --id.secret bankadminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/bank/tls-cert.pem
sleep 5

echo "Generate Bank peer MSP"
echo "=============================="
fabric-ca-client enroll -u https://peer0bank:peer0pw@localhost:8054 --caname ca-bank -M ${PWD}/organizations/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/msp --csr.hosts peer0.bank.funds.com --tls.certfiles ${PWD}/organizations/fabric-ca/bank/tls-cert.pem
sleep 5

echo "Generate Bank Peer tls cert"
echo "==================================="
fabric-ca-client enroll -u https://peer0bank:peer0pw@localhost:8054 --caname ca-bank -M ${PWD}/organizations/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls --enrollment.profile tls --csr.hosts peer0.bank.funds.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/bank/tls-cert.pem
sleep 5

echo "Organizing the folders"
echo "======================"

echo "Copy the certificate files"
echo "=========================="

cp organizations/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/tlscacerts/* organizations/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/ca.crt
cp organizations/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/signcerts/* organizations/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/server.crt
cp organizations/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/keystore/* organizations/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/server.key

mkdir -p ${PWD}/organizations/peerOrganizations/funds.com/bank.funds.com/msp/tlscacerts
cp ${PWD}/organizations/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/funds.com/bank.funds.com/msp/tlscacerts/ca.crt

mkdir -p ${PWD}/organizations/peerOrganizations/funds.com/bank.funds.com/tlsca
cp ${PWD}/organizations/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/funds.com/bank.funds.com/tlsca/tls-localhost-8054-ca-bank.pem

mkdir -p ${PWD}/organizations/peerOrganizations/funds.com/bank.funds.com/ca
cp ${PWD}/organizations/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/funds.com/bank.funds.com/ca/localhost-8054-ca-bank.pem

cp organizations/peerOrganizations/funds.com/bank.funds.com/msp/config.yaml organizations/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/msp/config.yaml

echo "Generate User MSP"
echo "================="

fabric-ca-client enroll -u https://user1:user1pw@localhost:8054 --caname ca-bank -M ${PWD}/organizations/peerOrganizations/funds.com/bank.funds.com/users/User1@bank.funds.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/bank/tls-cert.pem
sleep 5

cp organizations/peerOrganizations/funds.com/bank.funds.com/msp/config.yaml organizations/peerOrganizations/funds.com/bank.funds.com/users/User1@bank.funds.com/msp/config.yaml

echo "Generate org Admin MSP"
echo "======================"

fabric-ca-client enroll -u https://bankadmin:bankadminpw@localhost:8054 --caname ca-bank -M ${PWD}/organizations/peerOrganizations/funds.com/bank.funds.com/users/Admin@bank.funds.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/bank/tls-cert.pem
sleep 5

cp ${PWD}/organizations/peerOrganizations/funds.com/bank.funds.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/funds.com/bank.funds.com/users/Admin@bank.funds.com/msp/config.yaml


echo "==========================================================End of Bank ========================================================================================================"

echo "========================================================== Target ================================================================================================================="
echo "enroll Target ca "
echo "======================"

export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/funds.com/target.funds.com/

fabric-ca-client enroll -u https://admin:adminpw@localhost:9054 --caname ca-target --tls.certfiles ${PWD}/organizations/fabric-ca/target/tls-cert.pem
sleep 5





echo "Create config.yaml for Target"
echo "==================================="

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-target.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-target.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-target.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-target.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/funds.com/target.funds.com/msp/config.yaml

echo "Register target peer"
echo "=========================="
fabric-ca-client register --caname ca-target --id.name peer0target --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/target/tls-cert.pem
sleep 5

echo "Register target user"
echo "=========================="
fabric-ca-client register --caname ca-target --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/target/tls-cert.pem
sleep 5

echo "Register target Org admin"
echo "=========================="
fabric-ca-client register --caname ca-target --id.name targetadmin --id.secret targetadminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/target/tls-cert.pem
sleep 5

echo "Generate Target peer MSP"
echo "=============================="
fabric-ca-client enroll -u https://peer0target:peer0pw@localhost:9054 --caname ca-target -M ${PWD}/organizations/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/msp --csr.hosts peer0.target.funds.com --tls.certfiles ${PWD}/organizations/fabric-ca/target/tls-cert.pem
sleep 5

echo "Generate Target Peer tls cert"
echo "==================================="
fabric-ca-client enroll -u https://peer0target:peer0pw@localhost:9054 --caname ca-target -M ${PWD}/organizations/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls --enrollment.profile tls --csr.hosts peer0.target.funds.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/target/tls-cert.pem
sleep 5


echo "Organizing the folders"
echo "======================"

echo "Copy the certificate files"
echo "=========================="

cp organizations/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/tlscacerts/* organizations/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/ca.crt
cp organizations/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/signcerts/* organizations/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/server.crt
cp organizations/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/keystore/* organizations/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/server.key

mkdir -p ${PWD}/organizations/peerOrganizations/funds.com/target.funds.com/msp/tlscacerts
cp ${PWD}/organizations/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/funds.com/target.funds.com/msp/tlscacerts/ca.crt

mkdir -p ${PWD}/organizations/peerOrganizations/funds.com/target.funds.com/tlsca
cp ${PWD}/organizations/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/funds.com/target.funds.com/tlsca/tls-localhost-9054-ca-target.pem

mkdir -p ${PWD}/organizations/peerOrganizations/funds.com/target.funds.com/ca
cp ${PWD}/organizations/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/funds.com/target.funds.com/ca/localhost-9054-ca-target.pem

cp organizations/peerOrganizations/funds.com/target.funds.com/msp/config.yaml organizations/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/msp/config.yaml

echo "Generate User MSP"
echo "================="

fabric-ca-client enroll -u https://user1:user1pw@localhost:9054 --caname ca-target -M ${PWD}/organizations/peerOrganizations/funds.com/target.funds.com/users/User1@target.funds.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/target/tls-cert.pem
sleep 5

cp organizations/peerOrganizations/funds.com/target.funds.com/msp/config.yaml organizations/peerOrganizations/funds.com/target.funds.com/users/User1@target.funds.com/msp/config.yaml

echo "Generate org Admin MSP"
echo "======================"

fabric-ca-client enroll -u https://targetadmin:targetadminpw@localhost:9054 --caname ca-target -M ${PWD}/organizations/peerOrganizations/funds.com/target.funds.com/users/Admin@target.funds.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/target/tls-cert.pem
sleep 5

cp ${PWD}/organizations/peerOrganizations/funds.com/target.funds.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/funds.com/target.funds.com/users/Admin@target.funds.com/msp/config.yaml

echo "==========================================================End of Target ========================================================================================================================"

echo "==========================================================Orderer Config ========================================================================================================================"


echo "enroll Orderer ca"
echo "================="

mkdir -p organizations/ordererOrganizations/funds.com/orderer.funds.com/


export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/

fabric-ca-client enroll -u https://admin:adminpw@localhost:9052 --caname ca-orderer --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5
echo "Create config.yaml for Target "
echo "==================================="

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/msp/config.yaml

echo "Register orderer"
echo "=========================="
fabric-ca-client register --caname ca-orderer --id.name orderer0 --id.secret orderer0pw --id.type orderer --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

echo "Register orderer Org admin"
echo "=========================="
fabric-ca-client register --caname ca-orderer --id.name ordereradmin --id.secret ordereradminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

echo "Generate Orderer MSP"
echo "=============================="
fabric-ca-client enroll -u https://orderer0:orderer0pw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/orderers/orderer.funds.com/msp --csr.hosts orderer.funds.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

cp ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/orderers/orderer.funds.com/msp/config.yaml

echo "Generate Orderer TLS certificates"
echo "==================================="
fabric-ca-client enroll -u https://orderer0:orderer0pw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/orderers/orderer.funds.com/tls --enrollment.profile tls --csr.hosts orderer.funds.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5

echo "Organizing the folders"
echo "======================"
echo "Copy the certificate files"
echo "=========================="

  cp ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/orderers/orderer.funds.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/orderers/orderer.funds.com/tls/ca.crt
  cp ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/orderers/orderer.funds.com/tls/signcerts/* ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/orderers/orderer.funds.com/tls/server.crt
  cp ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/orderers/orderer.funds.com/tls/keystore/* ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/orderers/orderer.funds.com/tls/server.key

  mkdir -p ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/orderers/orderer.funds.com/msp/tlscacerts
  cp ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/orderers/orderer.funds.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/orderers/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem

  mkdir -p ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts
  cp ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/orderers/orderer.funds.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem

  cp ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/orderers/orderer.funds.com/msp/config.yaml

echo "Generate Orderer admin MSP"
echo "=========================="
  fabric-ca-client enroll -u https://ordereradmin:ordereradminpw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/users/Admin@funds.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5

  cp ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/funds.com/orderer.funds.com/users/Admin@funds.com/msp/config.yaml

echo "=========================================================End of Target ========================================================================================================================"







