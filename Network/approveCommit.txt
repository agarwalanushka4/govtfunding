
export ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem

export CHANNEL_NAME=govtchannel

******************** Approve for Government Org ***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=GovernmentMSP \
     -e CHANNEL_NAME=govtchannel \
     -e CORE_PEER_ADDRESS=peer0.government.funds.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/users/Admin@government.funds.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode approveformyorg --channelID govtchannel \
     --collections-config /opt/gopath/src/github.com/chaincode/GovFunding/collections.json \
     --name govfunds --version 1 --sequence 1 --signature-policy "OR('GovernmentMSP.peer', 'BankMSP.peer','TargetMSP.peer')" \
     --package-id govfunds_1:16baff923646a7e67051673a8820bd179bbe4c6951fd24b3be4517f7192e79bd \
     --tls --cafile $ORDERER_TLS_CA \
     --waitForEvent

******************** Approve for Bank Org ***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=BankMSP \
     -e CHANNEL_NAME=govtchannel \
     -e CORE_PEER_ADDRESS=peer0.bank.funds.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/users/Admin@bank.funds.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode approveformyorg --channelID govtchannel \
     --name govfunds --version 1 --sequence 1 --signature-policy "OR('GovernmentMSP.peer', 'BankMSP.peer','TargetMSP.peer')"\
     --collections-config /opt/gopath/src/github.com/chaincode/GovFunding/collections.json \
     --package-id govfunds_1:16baff923646a7e67051673a8820bd179bbe4c6951fd24b3be4517f7192e79bd  \
     --tls --cafile $ORDERER_TLS_CA --waitForEvent

******************** Approve for Target org ***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=TargetMSP \
     -e CHANNEL_NAME=govtchannel \
     -e CORE_PEER_ADDRESS=peer0.target.funds.com:11051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/target.funds.com/users/Admin@target.funds.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode approveformyorg --channelID govtchannel \
     --name govfunds --version 1 --sequence 1 --signature-policy "OR('GovernmentMSP.peer', 'BankMSP.peer','TargetMSP.peer')"\
     --collections-config /opt/gopath/src/github.com/chaincode/GovFunding/collections.json \
     --package-id govfunds_1:16baff923646a7e67051673a8820bd179bbe4c6951fd24b3be4517f7192e79bd  \
     --tls --cafile $ORDERER_TLS_CA --waitForEvent


******************** Check Commit Readiness ***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=GovernmentMSP \
     -e CHANNEL_NAME=govtchannel \
     -e CORE_PEER_ADDRESS=peer0.government.funds.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/users/Admin@government.funds.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode checkcommitreadiness --channelID govtchannel \
     --name govfunds --version 1 --sequence 1 --tls --cafile $ORDERER_TLS_CA --output json

******************** Commit Chaincode ***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=GovernmentMSP \
     -e CHANNEL_NAME=govtchannel \
     -e CORE_PEER_ADDRESS=peer0.government.funds.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/users/Admin@government.funds.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode commit -o orderer.funds.com:7050 -C govtchannel \
     --name govfunds --tls --cafile $ORDERER_TLS_CA \
     --peerAddresses peer0.government.funds.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/ca.crt \
     --peerAddresses peer0.bank.funds.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/ca.crt \
     --peerAddresses peer0.target.funds.com:11051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/target.funds.com/peers/peer0.target.funds.com/tls/ca.crt \
     --collections-config /opt/gopath/src/github.com/chaincode/GovFunding/collections.json \
     --version 1 --sequence 1 \
     --signature-policy "OR('GovernmentMSP.peer', 'BankMSP.peer','TargetMSP.peer')"





******************** Invoke Chaincode As Government Org Peer***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=GovernmentMSP \
     -e CHANNEL_NAME=govtchannel \
     -e CORE_PEER_ADDRESS=peer0.government.funds.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/users/Admin@government.funds.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer chaincode invoke -o orderer.funds.com:7050 --tls --cafile $ORDERER_TLS_CA \
     -C govtchannel -n govfunds \
     --peerAddresses peer0.government.funds.com:7051 \
     --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/government.funds.com/peers/peer0.government.funds.com/tls/ca.crt \
     -c '{"function":"createmoney", "Args":["0001","2000"]}'

******************** Invoke Chaincode As Bank Org Peer***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=BankMSP \
     -e CHANNEL_NAME=govtchannel \
     -e CORE_PEER_ADDRESS=peer0.bank.funds.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/funds.com/orderer.funds.com/msp/tlscacerts/tlsca.funds.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/users/Admin@bank.funds.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer chaincode invoke -o orderer.funds.com:7050 --tls --cafile $ORDERER_TLS_CA \
     -C govtchannel -n govfunds \
     --peerAddresses peer0.bank.funds.com:9051 \
     --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/funds.com/bank.funds.com/peers/peer0.bank.funds.com/tls/ca.crt \
     -c '{"function":"createmoney", "Args":["0002","500"]}'
