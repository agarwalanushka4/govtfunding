#!/bin/bash

function one_line_pem {
    echo "`awk 'NF {sub(/\\n/, ""); printf "%s\\\\\\\n",$0;}' $1`"
}

function json_ccp {
    local PP=$(one_line_pem $4)
    local CP=$(one_line_pem $5)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${P0PORT}/$2/" \
        -e "s/\${CAPORT}/$3/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        -e "s/\${ORGMSP}/$6/" \
        ccp-template.json
}

ORG=government
P0PORT=7051
CAPORT=7054
PEERPEM=../organizations/peerOrganizations/funds.com/government.funds.com/tlsca/tls-localhost-7054-ca-government.pem
CAPEM=../organizations/peerOrganizations/funds.com/government.funds.com/ca/localhost-7054-ca-government.pem
ORGMSP=Government

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $ORGMSP)" > gateways/connection-government.json

ORG=bank
P0PORT=9051
CAPORT=8054
PEERPEM=../organizations/peerOrganizations/funds.com/bank.funds.com/tlsca/tls-localhost-8054-ca-bank.pem
CAPEM=../organizations/peerOrganizations/funds.com/bank.funds.com/ca/localhost-8054-ca-bank.pem
ORGMSP=Bank

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $ORGMSP)" > gateways/connection-bank.json

ORG=target
P0PORT=11051
CAPORT=9054
PEERPEM=../organizations/peerOrganizations/funds.com/target.funds.com/tlsca/tls-localhost-9054-ca-target.pem
CAPEM=../organizations/peerOrganizations/funds.com/target.funds.com/ca/localhost-9054-ca-target.pem
ORGMSP=Target

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $ORGMSP)" > gateways/connection-target.json
