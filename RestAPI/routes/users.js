var express = require('express');
var router = express.Router();
const FabricCAServices = require('fabric-ca-client');
const { Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path')
const Joi = require('joi')
var jwt = require('jsonwebtoken');
var {applicationSecret} = require('./utils')


/* POST users listing. */
router.post('/', async function(req, res, next) {
  var userName = req.body.userName;
  var adminName = req.body.adminName;
  var role = req.body.role;
  const schema = Joi.object({
    userName: Joi.string().required(),
    adminName: Joi.string().required(),
    role: Joi.string().required().valid('client'),

    });
  
  const { error } = schema.validate(req.body);
  if(error) {
    res.status(500).send({ "status": "false", "message": `${error.message}` });

  } else {
  
  try {
    // load the network configuration
    const ccpPath = path.resolve(__dirname, '../../Network/organizations/gateways/connection-government.json')
    const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));

    // Create a new CA client for interacting with the CA.
    const caInfo = ccp.certificateAuthorities['ca.government.funds.com'];
    const caTLSCerts = caInfo.tlsCACerts.pem;
    const ca = new FabricCAServices(caInfo.url, { trustedRoots: caTLSCerts, verify: false },
        caInfo.caName);

    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), 'wallet');
    const wallet = await Wallets.newFileSystemWallet(walletPath);

    // Check to see if we've already enrolled the user.
    const userIdentity = await wallet.get(userName);
    if (userIdentity) {
        res.status(403).json({status:false, message:`Identity for the user ${userName} already found`})
        return;
    }

    // Check to see if we've already enrolled the admin user.
    const adminIdentity = await wallet.get(adminName);
    if (!adminIdentity) {
       res.status(404).json({status:false, message:`An identity for the admin user "admin" does not exist in the wallet`})
        return
    }

    // build a user object for authenticating with the CA
    const provider = wallet.getProviderRegistry().getProvider(adminIdentity.type);
    const adminUser = await provider.getUserContext(adminIdentity, adminName);

    // Register the user, enroll the user, and import the new identity into the wallet.

    const secret = await ca.register({
        enrollmentID: userName,
        role: role
    }, adminUser);
    const enrollement = await ca.enroll({
        enrollmentID: userName,
        enrollmentSecret: secret
    });
    const x509Identity = {
        credentials: {
            certificate: enrollement.certificate,
            privateKey: enrollement.key.toBytes()
        },
        mspId: 'GovernmentMSP',
        type: 'X.509'
    };
    await wallet.put(userName, x509Identity);
    res.status(200).json({status:true, message:`Successfully registered and enrolled the user ${userName}`})


} catch (error) {
    res.status(500).json({status:false, message:`Failed to register user ${userName}`})

   // process.exit(1);
}
  }
});

router.post('/login', async function(req, res, next) {
 var userName = req.body.userName
 const walletPath = path.join(process.cwd(), 'wallet');
    const wallet = await Wallets.newFileSystemWallet(walletPath);

    // Check to see if we've already enrolled the user.
    const userIdentity = await wallet.get(userName);
    if (userIdentity) {
      var token = jwt.sign({ userName: userName,exp: Math.floor(Date.now() / 1000) + (60 * 60),
      }, applicationSecret);
      console.log("token",token)
      res.status(200).json({status: true, token:`Bearer ${token}`})    
    } else {
      res.status(404).json({status: false, message:'User not found'})    

    }
  
})

module.exports = router;
