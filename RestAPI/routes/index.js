var express = require('express');
var router = express.Router();
const { Gateway, Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path')
const {clientApplication} = require('./client');
const Joi = require('joi')
var jwt = require('jsonwebtoken');
var {applicationSecret} = require('./utils')


router.get('/', function(req, res, next) {
    res.render('index', { title: 'Government Dashboard' });
})


/* GET home page. */
router.post('/money',async function(req, res, next) {
 
    let GovernmentClient = new clientApplication();

    //carId,make,model,color,DOM, manufacturer
    var userName = req.body.userName;
    var txName = 'createmoney';
    var moneyId = req.body.moneyId;
    var moneyValue = req.body.value;
     
    var decoded = jwtVerify(req)
    
    if(decoded.userName === userName) {

    const schema = Joi.object({
        userName: Joi.string().required(),
        moneyId: Joi.string().required(),
        moneyValue: Joi.string().required(),    
        });
      
      const { error } = schema.validate(req.body);
      if(error) {
        res.status(500).send({ "status": "false", "message": `${error.message}` });
    } else {
    GovernmentClient.generatedAndSubmitTxn(
        "government",
        userName,
        "govtchannel", 
        "govfunds",
        "MoneyContract",
        txName,
        moneyId,moneyValue
      ).then(message => {
          console.log("Message is $$$$",message)
          res.status(200).send({status:true, message: "Added Money"})
        }
      )
      .catch(error =>{
        console.log("Some error Occured $$$$###", error)
        res.status(500).send({status:false, error:`Failed to Add`,message:`${error}`})
      });
    }
} else {
    res.status(401).send("Unauthorized request")
}

});

router.get('/money', async function(req, res, next) {
    
    var carId = req.query.id;
    var userName = req.query.userName
    let GovernmentClient = new clientApplication();
    
    var decoded = jwtVerify(req)

    if(decoded.userName === userName) {
    GovernmentClient.generatedAndEvaluateTxn( 
        "government",
        userName,
        "govtchannel",
        "govfunds",
        "MoneyContract",
        "readmoney", moneyId)
        .then(message => {
          
          res.status(200).send({status:true, Moneydata : message.toString() });
        }).catch(error =>{
         
          res.status(500).send({status: false, error:`Failed to Add`,message:`${error}`})
        });
    } else {
        res.status(401).send("Unauthorized request")
    }
})

//For UI
router.post('/addMoney',async function(req, res, next) {
 
    let GovernmentClient = new clientApplication();

    var userName = req.body.userName;
    var txName = 'createmoney';
    var moneyId = req.body.moneyId;
    var moneyValue = req.body.value;
    console.log("money value", moneyValue);

    const schema = Joi.object({
        userName: Joi.string().required(),
        moneyId: Joi.string().required(),
        value: Joi.string().required(),  
        });
      
      const { error } = schema.validate(req.body);
      if(error) {
        res.status(500).send({ "status": "false", "message": `${error.message}` });
    } else {
      console.log("here");
    GovernmentClient.generatedAndSubmitTxn(
        "government",
        userName,
        "govtchannel", 
        "govfunds",
        "MoneyContract",
        txName,
        moneyId,moneyValue
      ).then(message => {
          console.log("Message is $$$$",message)
          res.status(200).send({status:true, message: "Added money"})
        }
      )
      .catch(error =>{
        console.log("Some error Occured $$$$###", error)
        res.status(500).send({status:false, error:`Failed to Add`,message:`${error}`})
      });
    }


});




const jwtVerify = (req) => {
    var token = req.headers.authorization;
    var jwtToken = token ? token.split(' ')[1] : ''
    try {
        var decoded = jwt.verify(jwtToken, applicationSecret);
        return decoded
      } catch(err) {

        return "error"
      }

}
//http://localhost:3000/car?id=01&&userName=appUser
    

module.exports = router;
