function FundWriteData(){
    event.preventDefault();
    const moneyId = document.getElementById('moneyID').value;
    const moneyValue = document.getElementById('value').value;
    const userName = document.getElementById('userName').value;
    console.log(moneyId+moneyValue+userName);

    if (moneyId.length==0||moneyValue.length==0||userName.length==0) {
        alert("You Missed Something")
        }
    else{
        fetch('/addMoney',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',              
            },
            body: JSON.stringify({moneyId: moneyId, value: moneyValue, userName: userName})
        })
        .then(function(response){
            if(response.status == 200) {
                alert("Added Money")
            } else {
                alert("Something Went wrong")
            }

        })
        .catch(function(error){
            alert("Something went wrong")
        })    
    }
}
